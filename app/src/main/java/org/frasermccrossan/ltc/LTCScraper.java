package org.frasermccrossan.ltc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

// everything required to load the LTC_supplied data into the database
@SuppressLint("UseSparseArrays")
public class LTCScraper {

	LoadDataTask task = null;
	ScrapingStatus status = null;
	Context context;

    static final String PROXY_BASE = "";
    static final Boolean PROXY_ENABLED = false;
    static final String LTC_BASE = "https://api.ltbuses.ca";
    static final String ROUTE_PATH = "/routes";
    public static final String ROUTE_URL = LTC_BASE + ROUTE_PATH; // used when calling the diagnostic screen
	static final String STOPS_PATH = "/stops";
    static final String PROXY_PREDICTION_PATH = "/WebWatch/ProxyMobileAda.aspx?r=%s&d=%s&s=%s";
    static final String LTC_PREDICTION_PATH = "/stop/%s";
	// matches arrival text in the MobileAda.aspx prediction
	static final Pattern ARRIVAL_PATTERN = Pattern.compile("(?i) *(\\d{1,2}:\\d{2} *[\\.apm]*) +(to .*)");
	// if no buses are found
	static final Pattern NO_INFO_PATTERN = Pattern.compile("(?mi)no stop information");
	static final Pattern NO_BUS_PATTERN = Pattern.compile("(?mi)no further buses");
	/* parseDocFromUri() starts with the initial timeout then retries doubling the timeout each time until
	 * greater than the maximum timeout, thus we get (for example) 1, 2, 4, 8, 16
	 */
	static final int INITIAL_FETCH_TIMEOUT = 2000;
	static final int MAXIMUM_FETCH_TIMEOUT = 128*1000;

	LTCScraper(Context c, ScrapingStatus s) {
		context = c;
		status = s;
	}

	LTCScraper(Context c) {
		/* instantiate this way if you plan only to check bus predictions
		 */
		context = c;
	}

	public void close() {
		if (task != null) {
			task.cancel(true);
		}
	}

	@SuppressLint("NewApi")
	public void loadAll() {
		task = new LoadDataTask();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			/* on Honeycomb and later, ASyncTasks run on a serial executor, and since
			 * we might have another asynctask running in an activity (e.g. fetching stop lists),
			 * we don't really want them all to block
			 */
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
		else {
			task.execute();
		}
	}

	/**
	 * Gets raw JSON from a URL.
	 * @param ltcPath
	 *   The route to the json file, relative to the LTC_BASE url.
	 * @return
	 *   Un-parsed raw JSON.
	 */
	private String fetchJsonFromUri(String ltcPath) {
		StringBuilder result = new StringBuilder();
		HttpURLConnection connection = null;

		try {
			URL url = new URL(LTC_BASE + ltcPath);
			connection = (HttpURLConnection) url.openConnection();
			InputStream in = new BufferedInputStream(connection.getInputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"), 8);
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}
		}
		catch (Exception e) {
			Log.e("jsonFetch", e.getMessage());
			return "";
		}
		finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

		return result.toString();
	}

	/**
	 * Loads predicted bus times for a stop.
	 * @param route
	 *   Useless.
	 * @param stopNumber
	 *   The (public) number of the bus stop.
	 * @param scrapeStatus
	 *   Scrapes status management object.
	 * @return
	 *   The prediction objects.
	 */
    public ArrayList<Prediction> getPredictions(LTCRoute route, String stopNumber, ScrapeStatus scrapeStatus) {
		ArrayList<Prediction> predictions = new ArrayList<>();
	    Resources res = context.getResources();
		String timesJson =fetchJsonFromUri(STOPS_PATH + "/" + stopNumber + "/" + route.getRouteNumber());
	    Calendar now = Calendar.getInstance();
	    now.set(Calendar.SECOND, 0);
	    now.set(Calendar.MILLISECOND, 0); // now we have 'now' set to the current time
		try {
			JSONArray predictionsData = new JSONArray(timesJson);
			for (int i = 0; i < predictionsData.length(); i += 1) {
				JSONObject predictionData = predictionsData.getJSONObject(i);
				String time = predictionData.getString("time");
				String destination = predictionData.getString("destination");
				predictions.add(new Prediction(route, time, destination, now));
			}
			if (predictions.isEmpty()) {
				throw new ScrapeException(res.getString(R.string.no_further), ScrapeStatus.PROBLEM_IF_ALL, false);
			}
			scrapeStatus.setStatus(ScrapeStatus.OK, ScrapeStatus.NOT_PROBLEM, null);
		}
		catch (JSONException e) {
			scrapeStatus.setStatus(ScrapeStatus.FAILED, ScrapeStatus.PROBLEM_IMMEDIATELY, "LTBuses Api Error.");
		}
		catch (ScrapeException e) {
			scrapeStatus.setStatus(ScrapeStatus.FAILED, e.problemType, e.getMessage());
			predictions.add(new Prediction(route, e.getMessage(), e.seriousProblem));
		}
		return predictions;
    }

	/**
	 * Loads stop data from the API.
	 * @return
	 *   The LTC stops.
	 */
	private ArrayList<LTCStop> loadStops() {
		ArrayList<LTCStop> stops = new ArrayList<>();
		String stopsJson = fetchJsonFromUri(STOPS_PATH);
		try {
			JSONArray stopsData = new JSONArray(stopsJson);
			for (int i = 0; i < stopsData.length(); i += 1) {
				JSONObject stopData = stopsData.getJSONObject(i);
				Integer number = Integer.parseInt(stopData.getString("number"));
				String name = stopData.getString("name");
				double latitude = Double.parseDouble(stopData.getString("lat"));
				double longitude = Double.parseDouble(stopData.getString("lon"));
				LTCStop stop = new LTCStop(number, name, latitude, longitude);

				// The routes are nested in another array.
				JSONArray routeNumbers = stopData.getJSONArray("routes");
				if (routeNumbers.length() > 0) {
					for (int j = 0; j < routeNumbers.length(); j += 1) {
						stop.linkRoute(routeNumbers.getString(j));
					}
				}

				stops.add(stop);
			}
		}
		catch (JSONException e) {
			Log.e("stops", e.getMessage());
		}
		return stops;
	}

	/**
	 * Loads route data from the API.
	 * @return
	 *   The routes
	 */
	private ArrayList<LTCRoute> loadRoutes() {
		ArrayList<LTCRoute> routes = new ArrayList<>();
		String routesJson = fetchJsonFromUri(ROUTE_PATH);
		try {
			JSONArray routesData = new JSONArray(routesJson);
			for (int i = 0; i < routesData.length(); i += 1) {
				JSONObject route = routesData.getJSONObject(i);
				String number = route.getString("number");
				String name = route.getString("name");
				routes.add(new LTCRoute(number, name));
			}
		}
		catch (JSONException e) {
			Log.e("routes", e.getMessage());
			return routes;
		}
		return routes;
	}

	private class LoadDataTask extends AsyncTask<Void, LoadProgress, Void> {

		protected Void doInBackground(Void... voids) {
			ArrayList<LTCRoute> routes;
			ArrayList<LTCStop> stops;

			Resources res = context.getResources();
			LoadProgress progress = new LoadProgress();
			BusDb db;

			try {
				publishProgress(progress.title(res.getString(R.string.downloading_routes)).percent(3));
				stops = loadStops();
				routes = loadRoutes();
				if (routes.size() == 0) {
					publishProgress(progress.title(res.getString(R.string.download_failed))
							.message(res.getString(R.string.no_routes_found))
							.failed());
				}
				else {
					publishProgress(progress.message(res.getString(R.string.saving_database)).percent(95));
					if (!isCancelled()) {
						db = new BusDb(context);
						db.saveBusData(routes, stops);
						db.close();
						publishProgress(progress.title(res.getString(R.string.stop_download_complete))
								.message(res.getString(R.string.database_ready))
								.percent(100).complete());
					}
				}
			}
			catch (SQLiteException e) {
				Log.e("updates", e.getMessage());
				publishProgress(progress.title(e.getMessage())
						.message("")
						.failed());
			}

			return null;
		}

		protected void onProgressUpdate(LoadProgress... progress) {
			if (!isCancelled() && status != null) {
				status.update(progress[0]);
			}
		}

	}

}
