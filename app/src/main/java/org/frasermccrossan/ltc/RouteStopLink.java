package org.frasermccrossan.ltc;

public class RouteStopLink {

	public String routeNumber;
	public Integer stopNumber;
	
	RouteStopLink(String route, Integer stop) {
		this.routeNumber = route;
		this.stopNumber = stop;
	}	

}
