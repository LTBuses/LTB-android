package org.frasermccrossan.ltc;

import java.util.ArrayList;

public class LTCStop {
	
	public Integer number;
	public String name;
	public double latitude;
	public double longitude;
	private ArrayList<RouteStopLink> routeLinks;

	LTCStop(Integer number, String name, double latitude, double longitude) {
		this(number, name);
		this.latitude = latitude;
		this.longitude = longitude;
	}

	LTCStop(Integer number, String name) {
		this.number = number;
		this.name = name;
		this.routeLinks = new ArrayList<RouteStopLink>();
	}

	public void linkRoute(String routeNumber) {
		this.routeLinks.add(new RouteStopLink(routeNumber, this.number));
	}

	public ArrayList<RouteStopLink> getRouteStopLinks () {
		return this.routeLinks;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
